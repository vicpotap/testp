//
//  Observerable.swift
//  Test
//
//  Created by Victor on 21/04/2019.
//  Copyright © 2019 Victor. All rights reserved.
//


import UIKit

class Observerable: NSObject {

    public static func activateMessage(nr:Int) {
        NotificationCenter.default.post(name:Notification.Name("PublicMessage"), object:nr)
    }
}
