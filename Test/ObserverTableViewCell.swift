//
//  ObserverTableCell.swift
//  Test
//
//  Created by Victor on 21/04/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit


class ObserverTableViewCell: UITableViewCell {
    
    @IBOutlet weak var activeLabel: UILabel!
    @IBOutlet weak var activeView: UIView!
    
    @IBOutlet weak var baseLabel: UILabel!
    @IBOutlet weak var baseView: UIView!
  
}
