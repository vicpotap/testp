//
//  Observer.swift
//  Test
//
//  Created by Victor on 21/04/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

protocol ReloadRowDelegate: AnyObject {
   func reloadRow(indexPath: IndexPath)
}

class Observer: NSObject {
    
    let id: Int
    var parenId: Int
    var indexPath: IndexPath?
 
    weak var delegate: ReloadRowDelegate?
    
    init(baseid: Int) {
        self.id = baseid
        self.parenId = baseid
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReceivedPublicMessage(notification:)), name: Notification.Name("PublicMessage"), object: nil)
    }
    
    @objc func ReceivedPublicMessage(notification: Notification) {
        guard let nr = notification.object as? Int else {
            self.parenId = 0
            return
        }
        self.parenId = nr
        
        guard let index = indexPath else {
            return
        }

        delegate?.reloadRow(indexPath: index)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("PublicMessage"), object: nil)
        
    }
}

