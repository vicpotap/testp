//
//  ViewController.swift
//  Test
//
//  Created by Victor on 21/04/2019.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ReloadRowDelegate{

    @IBOutlet weak var addObserverButton: UIButton!
    @IBOutlet weak var countObserverLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var order = 1
    private var observers: [Observer] = [] {
        didSet {
            tableView.reloadData()
            countObserverLabel.text = "You have \(observers.count) observers"        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     }

    @IBAction func addObserverAction(_ sender: Any) {
        let observer:Observer = Observer(baseid:order)
        observer.delegate = self
        observers.append(observer)
        order = order + 1
    }
    
    func reloadRow(indexPath: IndexPath)  {
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return observers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ObserverTableViewCell",
                                                 for: indexPath) as! ObserverTableViewCell
        
        guard indexPath.row <= observers.count else {
            return cell
        }
        
        let obsever = observers[indexPath.row]
        obsever.indexPath = indexPath
        cell.baseLabel.text = String(obsever.id)
        cell.activeLabel.text = String(obsever.parenId)
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            observers.remove(at: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row <= observers.count else {
            return
        }
        Observerable.activateMessage(nr:observers[indexPath.row].id)
    }
}
